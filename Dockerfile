FROM docker.io/library/python:3.9.20-slim-bookworm

RUN apt-get -y update && apt-get -y install jq && apt-get clean
COPY . /educationenricheducations/
WORKDIR /educationenricheducations
RUN pip install -r requirements.txt

CMD [ "python3", "/educationenricheducations/trigger_enrich_educations.py" ]
