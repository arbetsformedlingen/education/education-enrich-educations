import ndjson
import pytest

from educationenricheducations.enrich_educations import *

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def merged_educations():
    return load_ndjson_file(currentdir + 'resources/merged_educations_sample.jsonl')


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input_from_education_plan(merged_educations):
    education_id = 'i.uoh.su.hövka.21057.20222'
    expected_words = ['Kandidatprogram i språk och översättning, inriktning finska']
    assert_headline_input_words(education_id, expected_words, merged_educations)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input_from_education_plan_courses(merged_educations):
    education_id = 'i.uoh.su.hövka.21057.20222'
    expected_words = ['Översättning I', 'Översättning II', 'Översättningsvetenskap', 'Översättning', 'Tolkning']
    assert_headline_input_words(education_id, expected_words, merged_educations)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input_from_education_title(merged_educations):
    education_id = 'i.uoh.su.bkiv0.61638.20222'
    expected_words = ['magisterprogram i barnkultur']
    assert_headline_input_words(education_id, expected_words, merged_educations)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input_from_events(merged_educations):
    education_id = 'i.uoh.su.bkiv0.61638.20222'
    expected_words = ['kultur', 'rättigheter', 'barnrättskonvention', 'barnperspektiv', 'barn']

    assert_headline_input_words(education_id, expected_words, merged_educations)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_description_input_from_education_description(merged_educations):
    education_id = 'i.uoh.su.bkiv0.61638.20222'
    expected_words = ['Programmets övergripande mål är att erbjuda en tvärvetenskapligt']
    assert_description_input_words(education_id, expected_words, merged_educations)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_description_input_from_education_plan(merged_educations):
    education_id = 'i.uoh.su.hövka.21057.20222'
    from_ed_plan_brief = 'intresserad av att studera språk'
    from_ed_plan_description = 'Språken är engelska, finska, franska, italienska, lettiska'
    from_ed_plan_details = 'Under det första året läser du två obligatoriska kurser i språket'
    from_course_1_description = 'Du kommer också att få viss praktisk förtrogenhet med olika textgenrer'
    from_course_2_description = 'Kursen är avsedd för den som vill översätta till svenska från ett valfritt språk'

    expected_words = [from_ed_plan_brief, from_ed_plan_description, from_ed_plan_details, from_course_1_description,
                      from_course_2_description]
    assert_description_input_words(education_id, expected_words, merged_educations)

# @pytest.mark.skip(reason="Temporarily disabled")
def test_skip_blacklisted_terms_occupation(merged_educations):
    education_id = 'i.uoh.su.hv03ks.17010.20222'
    test_education = get_test_education_by_id(education_id, merged_educations)
    # description_input = get_doc_description_input(test_education)
    enriched_result = enrich_educations([test_education])

    assert enriched_result
    assert len(enriched_result) > 0
    for a_result in enriched_result:
        enriched_candidates = jmespath.search('text_enrichments_results.enriched_candidates', a_result)
        enriched_occupations = jmespath.search('occupations', enriched_candidates)
        assert 'modell' not in enriched_occupations

# @pytest.mark.skip(reason="Temporarily disabled")
def test_terms_for_tandskoterska_education(merged_educations):    
    education_id = 'i.myh.8006'
    test_education = get_test_education_by_id(education_id, merged_educations)
    enriched_result = enrich_educations([test_education])

    assert enriched_result
    assert len(enriched_result) > 0
    for a_result in enriched_result:
        enriched_candidates = jmespath.search('text_enrichments_results.enriched_candidates', a_result)
        enriched_occupations = jmespath.search('occupations', enriched_candidates)
        assert 'tandsköterska' in enriched_occupations
        
# @pytest.mark.skip(reason="Temporarily disabled")
def test_jobtitles_in_education(merged_educations):    
    education_id = 'i.uoh.liu.6mind.90124.20222'
    test_education = get_test_education_by_id(education_id, merged_educations)
    enriched_result = enrich_educations([test_education])

    assert enriched_result
    enriched_occupations = jmespath.search('text_enrichments_results.enriched_candidates.occupations', enriched_result[0])
    assert len(enriched_occupations) == 0

        
# @pytest.mark.skip(reason="Temporarily disabled")
def test_enrich_sjukskoterskeprogrammet_education_headline(merged_educations):
    education_id = 'i.uoh.uu.msj1y.p3500.20222'
    test_education = get_test_education_by_id(education_id, merged_educations)

    # Set education.description and education_plan to empty values to make sure that we find 'sjuksköterska' in the headline.
    test_education['education']['description'] = [{'lang': 'swe', 'content': 'En ny beskrivning utan yrkestiteln.'}]
    test_education['education_plan']= {}

    enriched_result = enrich_educations([test_education])

    assert enriched_result
    assert len(enriched_result) > 0
    for a_result in enriched_result:
        enriched_candidates = jmespath.search('text_enrichments_results.enriched_candidates', a_result)
        enriched_occupations = jmespath.search('occupations', enriched_candidates)
        assert 'sjuksköterska' in enriched_occupations

# @pytest.mark.skip(reason="Temporarily disabled")
def test_occupation_threshold(merged_educations):
    education_id = 'i.myh.7743'
    test_education = get_test_education_by_id(education_id, merged_educations)
    # description_input = get_doc_description_input(test_education)
    enriched_result = enrich_educations([test_education])

    assert enriched_result
    assert len(enriched_result) > 0
    for a_result in enriched_result:
        enriched_candidates = jmespath.search('text_enrichments_results.enriched_candidates', a_result)
        enriched_occupations = jmespath.search('occupations', enriched_candidates)
        assert 'fastighetsförvaltare' in enriched_occupations

        assert 'föreläsare' not in enriched_occupations
        assert 'entreprenör' not in enriched_occupations
        assert 'lärare' not in enriched_occupations


# @pytest.mark.skip(reason="Temporarily disabled")
def test_enrich_output_structure(merged_educations):
    education_id = 'i.myh.7743'
    test_education = get_test_education_by_id(education_id, merged_educations)
    # description_input = get_doc_description_input(test_education)
    enriched_result = enrich_educations([test_education])

    assert enriched_result
    assert len(enriched_result) > 0
    for a_result in enriched_result:
        text_enrichments_result = jmespath.search('text_enrichments_results', a_result)
        enriched_candidates = jmespath.search('enriched_candidates', text_enrichments_result)
        assert len(enriched_candidates) > 0

        enriched_candidates_complete = jmespath.search('enriched_candidates_complete', text_enrichments_result)
        assert len(enriched_candidates_complete) > 0

def assert_description_input_words(education_id, expected_words, merged_educations):
    test_education = get_test_education_by_id(education_id, merged_educations)
    description_input = get_doc_description_input(test_education)
    assert description_input
    # log.info('education_id: %s - description_input: %s' % (education_id, description_input))
    description_input_lower = description_input.lower()
    # Make sure that words was found in the json-structure.
    for expected_word in expected_words:
        expected_word_lower = expected_word.lower()
        assert expected_word_lower in description_input_lower


def assert_headline_input_words(education_id, expected_words, merged_educations):
    test_education = get_test_education_by_id(education_id, merged_educations)
    headline_input = get_doc_headline_input(test_education)
    assert headline_input
    # log.info('education_id: %s - headline_input: %s' % (education_id, headline_input))
    headline_input_lower = headline_input.lower()
    # Make sure that words was found in the json-structure.
    for expected_word in expected_words:
        expected_word_lower = expected_word.lower()
        assert expected_word_lower in headline_input_lower


def get_test_education_by_id(id, dicts):
    return next((item for item in dicts if item["id"] == id), None)


def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data


if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])
