import os

S3_URL = os.getenv('S3_URL', 'https://minio.arbetsformedlingen.se')
S3_BUCKET_NAME = os.getenv('S3_BUCKET_NAME', 'kll-test')
AWS_ACCESS_KEY = os.getenv('AWS_ACCESS_KEY', 'kll-test')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY', 'your Minio password here')

#  Use pattern that matches for example: merged_educations_2022-03-03_02_52_49.jsonl
ENRICH_EDUCATIONS_S3_SRC_FILE_NAME_PATTERN = os.getenv('ENRICH_EDUCATIONS_S3_SRC_FILE_NAME_PATTERN', 'merged_educations_\\d{4}-\\d{2}-\\d{2}.*\\.jsonl')
# ENRICH_EDUCATIONS_S3_SRC_FILE_NAME_PATTERN = os.getenv('ENRICH_EDUCATIONS_S3_SRC_FILE_NAME_PATTERN', 'merged_educations_sample.jsonl')
ENRICH_EDUCATIONS_S3_SRC_FILE_MAX_VALID_HOURS = os.getenv('ENRICH_EDUCATIONS_S3_SRC_FILE_MAX_VALID_HOURS', '24')

ENRICH_EDUCATIONS_S3_DEST_FILE_NAME_PREFIX = os.getenv('ENRICH_EDUCATIONS_S3_DEST_FILE_NAME_PREFIX', 'merged_and_enriched_educations')

# For local storage only while developing, as an alternative to Minio.
ENRICH_EDUCATIONS_LOCAL_RELATIVE_FILE_PATH = os.getenv('ENRICH_EDUCATIONS_LOCAL_RELATIVE_FILE_PATH', '../educationenricheducations/enriched-educations.jsonl')
ENRICH_EDUCATIONS_SAVE_RESULT_IN_FILE_ONLY = os.getenv('ENRICH_EDUCATIONS_SAVE_RESULT_IN_FILE_ONLY', 'false').lower() == 'true'

ENRICH_EDUCATIONS_OCCUPATIONS_THRESHOLD = os.getenv('ENRICH_EDUCATIONS_OCCUPATIONS_THRESHOLD', '0.8')

ENRICH_CALLS_PARALLELISM = int(os.getenv('ENRICH_CALLS_PARALLELISM', '4'))
# JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-api.jobtechdev.se')
JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-test-api.jobtechdev.se')
JAE_API_TIMEOUT_SECONDS = int(os.getenv('JAE_API_TIMEOUT_SECONDS', '60'))

