import logging

from educationenricheducations.enrich_educations import enrich_educations_and_persist


class EnrichEducationsStarter(object):

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

    def start(self):
        self.log.info('Starting enrich educations and persist process..')
        enrich_educations_and_persist()
