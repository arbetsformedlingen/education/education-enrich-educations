import concurrent.futures
import copy
import json
import logging
import os
from datetime import datetime
from multiprocessing import Value

import itertools
import jmespath
import requests
import sys
import time

from educationenricheducations import settings
from educationenricheducations.decorators import timeit
from educationenricheducations.helpers import load_blacklist_file
from educationenricheducations.minio_downloader import MinioDownloader
from educationenricheducations.minio_uploader import MinioUploader

CANDIDATE_TYPE_OCCUPATIONS = 'occupations'

log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

counter = Value('i', 0)

ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'
ENRICHER_RETRIES = 10

NR_OF_ITEMS_PER_BATCH = 20

minio_downloader = MinioDownloader()



def replace_suffix_from_str(str, replace_suffix, new_suffix):
    if not str or str == '':
        return str

    if str.endswith(replace_suffix):
        pos= str.rfind(replace_suffix)
        if pos > -1:
            str = str[:pos] + new_suffix + str[pos + len(replace_suffix):]
    return str

# Extract education words (not terms) from education title.
# Return a list of extracted words from title.
def extract_occupations_from_education_title(title_list):

    if not title_list or len(title_list) < 1:
        return title_list

    extracted_occupation_suggestions = []
    for title_word in title_list:
        title_word = title_word.strip()
        title_word = title_word.lower()
        suggested_title_word = title_word

        # Remove as suffix, e.g. tandläkarprogrammet -> tandläkar
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'programmet', '')
        # Remove as suffix, e.g. humanistprogram -> humanist
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'program', '')
        # Remove as suffix, e.g. flygteknikutbildningen  -> flygteknik
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'utbildningen', '')
        # Remove as suffix, e.g. flygteknikutbildning  -> flygteknik
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'utbildning', '')
        # Remove as suffix, e.g. itkursen  -> it
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'kursen', '')
        # Remove as suffix, e.g. itkurser  -> it
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'kurser', '')
        # Remove as suffix, e.g. itkurs  -> it
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'kurs', '')
        # Remove as suffix, e.g. Musikteaterlinjen -> musikteater
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'linjen', '')
        # Remove as suffix, e.g. Musikteaterlinje -> musikteater
        suggested_title_word = replace_suffix_from_str(suggested_title_word, 'linje', '')

        if suggested_title_word != title_word:
            # Only rewrite the remainder if the suffix has been rewritten already.

            # Remove as suffix, e.g. läkar -> läkare
            suggested_title_word = replace_suffix_from_str(suggested_title_word, 'ar', 'are')
            # Remove as suffix, e.g. sjuksköterske -> sjuksköterska
            suggested_title_word = replace_suffix_from_str(suggested_title_word, 'ske', 'ska')
            # Remove as suffix, e.g. vallning -> vallare
            suggested_title_word = replace_suffix_from_str(suggested_title_word, 'ning', 'are')
            # Remove as suffix, e.g. grävmaskin -> grävmaskinist
            suggested_title_word = replace_suffix_from_str(suggested_title_word, 'maskin', 'maskinist')
            # Remove as suffix, e.g. fysioterapi -> fysioterapeut
            suggested_title_word = replace_suffix_from_str(suggested_title_word, 'terapi', 'terapeut')
            # Remove as suffix, e.g. maskinoperation -> maskinoperatör
            suggested_title_word = replace_suffix_from_str(suggested_title_word, 'tion', 'tör')

        if suggested_title_word != title_word:
            extracted_occupation_suggestions.append(suggested_title_word)

    return extracted_occupation_suggestions

def get_doc_headline_input(education):
    sep = ' | '
    headline_values = []

    education_title = jmespath.search("education.title[?lang=='swe'].content | [0]", education)
    if education_title:
        headline_values.append(education_title)

    education_plan_title = jmespath.search("education_plan.title", education)
    if education_plan_title:
        headline_values.append(education_plan_title)

    education_plan_courses = jmespath.search("education_plan.courses[*]", education)
    if education_plan_courses:
        for education_plan_course in education_plan_courses:
            course_title = jmespath.search("course.title", education_plan_course)
            if course_title:
                headline_values.append(course_title)

            education_subject_labels = jmespath.search("course.meta.educationSubjects[*].label", education_plan_course)
            if education_subject_labels:
                headline_values.extend(education_subject_labels)
            new_keywords_words = jmespath.search("course.meta.nyaKeywords[0].words[*]", education_plan_course)
            if new_keywords_words:
                headline_values.extend(new_keywords_words)

    events = jmespath.search("events", education)
    if events:
        for event in events:
            if 'extension' in event:
                if 'keywords' in event['extension']:
                    keywords_swe = jmespath.search("extension.keywords[?lang=='swe'].content", event)
                    if keywords_swe:
                        # Some keywords are numeric, like: 2030
                        keywords_swe = [str(keyword) for keyword in keywords_swe]
                        log.debug('keywords_swe: %s' % keywords_swe)
                        headline_values.extend(keywords_swe)

    headline_values.extend(extract_occupations_from_education_title(headline_values))

    doc_headline_input = sep.join(set(headline_values))

    log.debug("doc_headline_input: %s" % doc_headline_input)

    return doc_headline_input


def get_doc_description_input(education):
    sep = '\n'
    description_values = []

    education_description = jmespath.search("education.description[?lang=='swe'].content | [0]", education)
    if education_description:
        description_values.append(education_description)

    education_plan_brief = jmespath.search("education_plan.brief", education)
    if education_plan_brief:
        description_values.append(education_plan_brief)

    education_plan_description = jmespath.search("education_plan.description", education)
    if education_plan_description:
        description_values.append(education_plan_description)

    education_plan_details = jmespath.search("education_plan.details", education)
    if education_plan_details:
        description_values.append(education_plan_details)

    education_plan_courses = jmespath.search("education_plan.courses[*]", education)
    if education_plan_courses:
        for education_plan_course in education_plan_courses:
            course_description = jmespath.search("course.description", education_plan_course)
            if course_description:
                description_values.append(course_description)

    if description_values:
        complete_description_text = sep.join(description_values)
    else:
        complete_description_text = ''

    log.debug("doc_description_input: %s" % education_description)

    return complete_description_text


def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])


def get_enrich_result(batch_indata, jae_api_endpoint_url, timeout):
    log.info('Getting result from endpoint: %s' % jae_api_endpoint_url)

    headers = {'Content-Type': 'application/json'}

    for retry in range(ENRICHER_RETRIES):
        try:
            r = requests.post(url=jae_api_endpoint_url, headers=headers, json=batch_indata, timeout=timeout)
            r.raise_for_status()
        except Exception as e:
            log.info(f"get_enrich_result() retrying #{retry + 1} after error: {e}")
            doc_ids_in_batch = jmespath.search('documents_input[*].doc_id', batch_indata)
            log.info(f"List of doc_id in failing batch: {doc_ids_in_batch}")
            time.sleep(0.5)
        else:
            return r.json()
    log.error(f"_get_enrich_result failed after: {ENRICHER_RETRIES} retries with error. Exit!")
    sys.exit(1)


def execute_calls(batch_indatas, jae_api_endpoint_url, parallelism):
    global counter
    enriched_output = {}
    # Use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=parallelism) as executor:
        # Start the operations and mark each future with its URL
        future_to_enrich_result = {executor.submit(get_enrich_result, batch_indata, jae_api_endpoint_url,
                                                   settings.JAE_API_TIMEOUT_SECONDS): batch_indata
                                   for batch_indata in batch_indatas}
        for future in concurrent.futures.as_completed(future_to_enrich_result):
            try:
                enriched_result = future.result()
                for resultrow in enriched_result:
                    enriched_output[resultrow[ENRICHER_PARAM_DOC_ID]] = resultrow
                    with counter.get_lock():
                        counter.value += 1
                        if counter.value % 1000 == 0:
                            log.info(f'enrichtextdocuments - Processed docs: {counter.value}')
            except Exception as e:
                log.error(e)
                raise

    return enriched_output


def enrich(ad_batches, batch_indata_config, endpoint_url, parallelism):
    batch_indatas = []
    for i, ad_batch in enumerate(ad_batches):
        ad_batch_indatas = [ad_indata for ad_indata in ad_batch]
        batch_indata = copy.deepcopy(batch_indata_config)
        batch_indata['documents_input'] = ad_batch_indatas
        batch_indatas.append(batch_indata)
    log.info('Will enrich with %s batch_indatas' % len(batch_indatas))
    enrich_results_data = execute_calls(batch_indatas, endpoint_url, parallelism)

    return enrich_results_data


def get_empty_output_value():
    return {
        "enriched_candidates": {
            "occupations": [],
            "competencies": [],
            "traits": [],
            "geos": []
        },
        "enriched_candidates_complete": {
            "occupations": [],
            "competencies": [],
            "traits": [],
            "geos": []
        }
    }


def extract_and_sort_concept_labels(enriched_candidates):
    labels = [candidate['concept_label'].lower() for candidate in enriched_candidates]
    return sorted(labels)


def add_enriched_result(educations_to_update, enriched_results_data):

    blacklist_terms_dict = load_blacklist_terms()

    occupations_thresh = float(settings.ENRICH_EDUCATIONS_OCCUPATIONS_THRESHOLD)

    for education_to_update in educations_to_update:
        if not 'text_enrichments_results' in education_to_update:
            education_to_update['text_enrichments_results'] = {}
        doc_id = str(education_to_update.get('id', ''))
        if doc_id in enriched_results_data:
            enriched_results = enriched_results_data[doc_id]
            enriched_results_candidates = jmespath.search('enriched_candidates', enriched_results)

            enriched_output = get_empty_output_value()
            enriched_output_candidates = enriched_output['enriched_candidates']
            enriched_output_candidates_complete = enriched_output['enriched_candidates_complete']
            for attr_name in enriched_results_candidates.keys():
                enriched_candidates_for_type = jmespath.search(attr_name, enriched_results_candidates)

                if attr_name == CANDIDATE_TYPE_OCCUPATIONS:
                    # Only add occupations if they have a prediction above threshold.
                    filtered_enriched_candidates_for_type = [candidate for candidate in enriched_candidates_for_type if candidate['prediction'] > occupations_thresh]
                    candidates_in_headline = [candidate for candidate in enriched_candidates_for_type if candidate[
                        'sentence_index'] == 0 and candidate not in filtered_enriched_candidates_for_type]
                    # Also add occupations from the headline, even if they are below prediction threshold.
                    filtered_enriched_candidates_for_type = filtered_enriched_candidates_for_type + candidates_in_headline
                else:
                    filtered_enriched_candidates_for_type = enriched_candidates_for_type

                formated_labels = extract_and_sort_concept_labels(filtered_enriched_candidates_for_type)
                blacklist_terms_dict_for_education = append_blacklist_terms_for_education_form(education_to_update, blacklist_terms_dict)
                cleaned_candidates = [term for term in formated_labels if term not in blacklist_terms_dict_for_education[attr_name]]

                enriched_output_candidates_complete[attr_name] = cleaned_candidates

                unique_candidates_for_type = sorted(list(set(cleaned_candidates)))
                enriched_output_candidates[attr_name] = unique_candidates_for_type
        else:
            # Set non valid education to empty enriched values.
            enriched_output = get_empty_output_value()



        education_to_update['text_enrichments_results'] = enriched_output


def load_blacklist_terms():
    candidates_type_names = ['competencies', 'occupations', 'traits', 'geos']
    blacklist_terms_dict = {}
    for type_name in candidates_type_names:
        blacklist_filepath = f'{currentdir}resources/blacklist_{type_name}.txt'
        blacklist_terms = set(load_blacklist_file(blacklist_filepath))
        blacklist_terms_dict[type_name] = blacklist_terms
    return blacklist_terms_dict

def append_blacklist_terms_for_education_form(education = None, blacklist_terms_dict =None):
    if not blacklist_terms_dict:
        blacklist_terms_dict = {}
    # Education forms (i.e. in the black list) should not be used as enriched terms
    # (E.g vuxenutbildning is not a competence for a student on SFI)
    candidates_type_names = ['competencies', 'occupations', 'traits', 'geos']
    if education:
        blacklist_forms = ["gymnasieskola","sfi","grundskola", "särvux", "komvux", "specialskola", "särskola", "vuxenutbildning"]
        if jmespath.search("education.form.code", education):
            education_form_code = jmespath.search("education.form.code", education).lower()
            if education_form_code in blacklist_forms:
                for blacklist_form in blacklist_forms:
                    for type_name in candidates_type_names:
                        blacklist_terms_dict[type_name].add(blacklist_form)
    return blacklist_terms_dict

def enrich_educations(educations_to_enrich):
    parallelism = settings.ENRICH_CALLS_PARALLELISM
    if parallelism <= 0:
        parallelism = 1

    log.info(f'Before allocating variable educations')
    educations = [education for education in educations_to_enrich]
    log.info(f'Done allocating variable educations' )

    log.info(f'Preparing input for enriching, parallelism = {parallelism}' )

    ads_input_data = []
    for education in educations:
        doc_id = str(education.get('id', ''))

        doc_headline = get_doc_headline_input(education)
        doc_text = get_doc_description_input(education)

        input_doc_params = {
            ENRICHER_PARAM_DOC_ID: doc_id,
            ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
            ENRICHER_PARAM_DOC_TEXT: doc_text
        }

        ads_input_data.append(input_doc_params)
    ad_batches = grouper(NR_OF_ITEMS_PER_BATCH, ads_input_data)
    batch_indata_config = {
        "include_terms_info": True,
        "include_sentences": True,
        "sort_by_prediction_score": "NOT_SORTED"
    }

    add_enriched_result(educations, enrich(ad_batches, batch_indata_config, settings.JAE_API_URL + '/enrichtextdocuments', parallelism))
    return educations

@timeit
def enrich_educations_and_persist():
    log.info('Starting to enrich data from educations and education plans..')

    if settings.ENRICH_EDUCATIONS_SAVE_RESULT_IN_FILE_ONLY:
        if not settings.ENRICH_EDUCATIONS_LOCAL_RELATIVE_FILE_PATH:
            raise RuntimeError(
                'Env variable OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY is True but OCCUPATIONS_RELATIVE_FILE_PATH has not been set.')

        log.info(
            'Will save the result in file only, with filepath: %s' % currentdir + settings.ENRICH_EDUCATIONS_LOCAL_RELATIVE_FILE_PATH)
    else:
        log.info('Will save the result in Minio, bucket: %s' % (settings.S3_BUCKET_NAME))

    enriched_educations = enrich_educations(minio_downloader.download_educations_from_file())

    if settings.ENRICH_EDUCATIONS_SAVE_RESULT_IN_FILE_ONLY:
        complete_file_path = currentdir + settings.ENRICH_EDUCATIONS_LOCAL_RELATIVE_FILE_PATH
        log.info('Writing enriched occupations to file: %s' % complete_file_path)
        write_items_to_file(enriched_educations, complete_file_path)
    else:
        save_enriched_educations_in_repository(enriched_educations)



def write_items_to_file(json_src, filepath):
    file = open(filepath, 'w', encoding='utf-8')
    for record in json_src:
        file.write(json.dumps(record) + '\n')
    file.close()


def save_enriched_educations_in_repository(enriched_educations):
    time_stamp = datetime.now().strftime("%Y-%m-%d_%I_%M_%S")
    filename_minio = f'{settings.ENRICH_EDUCATIONS_S3_DEST_FILE_NAME_PREFIX}_{time_stamp}.jsonl'

    log.info(f'Will save enriched educations as filename: {filename_minio}')
    minio_uploader = MinioUploader()

    minio_uploader.upload_json_data(enriched_educations, filename_minio)

# educations = minio_downloader.download_educations_from_file()
# print(len([education for education in educations]))

# enrich_educations_and_persist()
