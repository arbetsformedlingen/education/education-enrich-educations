import json
import logging
import re
import threading
from datetime import datetime
from tempfile import TemporaryFile

import boto3
import sys
from botocore.client import Config

from educationenricheducations import settings


class MinioDownloader(object):
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY
    s3_src_file_max_valid_hours = settings.ENRICH_EDUCATIONS_S3_SRC_FILE_MAX_VALID_HOURS

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

        self.s3_client = boto3.client('s3',
                                      endpoint_url=self.s3_url,
                                      aws_access_key_id=self.aws_access_key,
                                      aws_secret_access_key=self.aws_secret_access_key,
                                      config=Config(signature_version='s3v4'),
                                      region_name='us-east-1')

    def download_educations_from_file(self):
        ad_src_file_name = self._get_filtered_filename_in_bucket()

        with TemporaryFile() as data:
            response = self.s3_client.head_object(Bucket=self.s3_bucket_name, Key=ad_src_file_name)
            filesize = response['ContentLength']
            self.log.info('Starting download for file %s with filesize: %s' % (ad_src_file_name, filesize))
            self.s3_client.download_fileobj(self.s3_bucket_name, ad_src_file_name, data,
                                            Callback=ProgressPercentage(ad_src_file_name, filesize))
            self.log.info('Downloaded file %s' % ad_src_file_name)
            line_counter = 0
            data.seek(0)
            for line in data.readlines():
                try:
                    line_counter += 1
                    decoded_line = line.decode("utf-8").strip()
                    if line_counter % 1000 == 0:
                        self.log.info(f'{line_counter} lines returned from downloaded file: {ad_src_file_name}')
                    yield json.loads(decoded_line)
                except Exception as e:
                    self.log.error(f"Error when parsing line, line_counter:{line_counter}, exception: {e}")
                    self.log.error(f"Raw line: {line}")

    def _get_filtered_filename_in_bucket(self):
        object_list = self.s3_client.list_objects_v2(
            Bucket=self.s3_bucket_name
        )
        RE_WANTED_FILE_PATTERN = re.compile(settings.ENRICH_EDUCATIONS_S3_SRC_FILE_NAME_PATTERN, re.UNICODE)

        fileobjects = object_list['Contents']
        time_now = datetime.now()

        filtered_fileobjects = []
        for fileobject in fileobjects:
            filename = fileobject['Key']
            filesize = fileobject['Size']

            if RE_WANTED_FILE_PATTERN.match(filename) and filesize > 0 and self.check_is_valid_timestamp(fileobject,
                                                                                                         time_now):
                filtered_fileobjects.append(fileobject)

        if not filtered_fileobjects:
            raise FileExistsError(
                'No file could be found according to the regex pattern: %s and a timestamp within %s hours' % (
                    settings.ENRICH_EDUCATIONS_S3_SRC_FILE_NAME_PATTERN, settings.ENRICH_EDUCATIONS_S3_SRC_FILE_MAX_VALID_HOURS))

        #  Sort on last_modified desc to get the latest file according to the filename pattern.
        sorted_filtered_fileobjects = sorted(filtered_fileobjects, key=lambda d: d['LastModified'], reverse=True)
        wanted_file_name = sorted_filtered_fileobjects[0]['Key']

        return wanted_file_name

    def check_is_valid_timestamp(self, fileobject, time_now):
        filelastmodified = fileobject['LastModified']
        # Remove timezone so the datetime can be compared to now_time.
        filelastmodified = filelastmodified.replace(tzinfo=None)
        difference = time_now - filelastmodified
        diff_in_hours = difference.total_seconds() / 3600

        is_valid_timestamp = (diff_in_hours <= float(self.s3_src_file_max_valid_hours))
        if not is_valid_timestamp:
            self.log.debug('File %s is %s hours old right now and is not valid.' % (fileobject['Key'], round(diff_in_hours, 2)))

        return is_valid_timestamp


class ProgressPercentage(object):
    def __init__(self, filename, filesize):
        self._filename = filename
        self._size = float(filesize)
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\rDownloading %s  %s / %s  (%.2f%%)" % (
                    self._filename, self._seen_so_far, self._size,
                    percentage))
            sys.stdout.flush()
