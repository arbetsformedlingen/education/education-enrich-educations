import logging
import os
from os.path import isfile

import itertools
import ndjson

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data

def load_blacklist_file(blacklist_filepath):
    if not isfile(blacklist_filepath):
        return []

    with open(blacklist_filepath, encoding='utf-8') as stoplistfile:
        blacklist_terms = stoplistfile.readlines()
    blacklist_terms = [x.strip() for x in blacklist_terms]
    return blacklist_terms

def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])

